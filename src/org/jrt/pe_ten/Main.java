/**
 * org.jrt.pe_ten is a list of operations to complete project euler problem ten
 */
package org.jrt.pe_ten;

import java.math.BigInteger;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * 
 * Find the sum of all the primes below two million.
 * 
 * @author jrtobac
 *
 */
public class Main {

	public static void main(String[] args) {
		BigInteger prime = new BigInteger("2");
		BigInteger total = new BigInteger("0");
		BigInteger twoMill = new BigInteger("2000000");
		
		while(prime.compareTo(twoMill) <= 0) {
			total = total.add(prime);
			prime = prime.nextProbablePrime();
		}
		
		System.out.println(total);

	}

}
